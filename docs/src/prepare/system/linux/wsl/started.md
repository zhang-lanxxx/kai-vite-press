---
title: WSL2 安装与配置
index: 0
---

## 什么是 WSL?

::: tip 官方描述
Windows Subsystem for Linux ( WSL ) 可让开发人员按原样运行 GNU/Linux 环境 - 包括大多数命令行工具、实用工具和应用程序 -
且不会产生传统虚拟机或双启动设置开销。
:::

简而言之，`WSL` 就是可以直接运行在 `Windows` 系统上的 `Linux` 子系统

- 默认提供 `Ubuntu`、`Debian` 等多个发行版选择。
- 支持任意 `Linux` 发行版导入如 `CentOS`、`Fedora Core` 等。
- `Windows` 与 `WSL` 环境变量共享，命令互通，这对开发者来说很友好。
- 启动时资源管理器会挂载 `Linux` 子系统文件目录。
- 支持 `Intellij IDEA`、`Webstorm` 等编辑器远程开发调试。

## 版本对比

WSL 目前有两个版本 : WSL1 和 WSL2， WSL2 是 WSL1 的升级版，主要提高了 `文件系统性能` 及 `系统调用兼容性` ，功能对比如下 :

|                功能                 | WSL1 | WSL2 |
|:---------------------------------:|:----:|:----:|
|       Windows 和 Linux 之间的集成       |  ✅   |  ✅   |
|               启动时间短               |  ✅   |  ✅   |
|         与传统虚拟机相比，占用的资源量少          |  ✅   |  ✅   |
| 可以与当前版本的 VMware 和 VirtualBox 一起运行 |  ✅   |  ✅   |
|               托管 VM               |  ❌   |  ✅   |
|           完整的 Linux 内核            |  ❌   |  ✅   |
|            完全的系统调用兼容性             |  ❌   |  ✅   |
|           跨 OS 文件系统的性能            |  ✅   |  ❌   |

从上面的对比不难看出，`WSL 2` 在几个方面优于 `WSL 1`，但跨 OS 文件系统的性能除外，对于这种情况，需要尽可能保证文件存储及文件操作在同一操作系统上进行。

## 查看系统是否支持 WSL 2

::: warning 提示
WSL2 仅支持 Windows 10 版本 2004 及更高版本（内部版本 19041 及更高版本）或 Windows 11。
:::

```shell
# powershell 输入如下命令，获取操作系统内部版本
$host.Version.Build
```

内部版本号大于 `19041` 说明支持 WSL 2 安装，小于 `19041`
请参考 [旧版 WSL 手动安装步骤](https://learn.microsoft.com/zh-cn/windows/wsl/install-manual)

## WSL 常用命令

参考自 [WSL 的基本命令](https://learn.microsoft.com/zh-cn/windows/wsl/basic-commands)

### 安装与卸载

::: warning 注意
取消注册后，此发行版关联的所有数据、设置和软件将一并删除，建议做好数据备份。
:::

```shell
# 默认发行版安装, 此命令默认安装 WSL 及 `Ubuntu` 发行版
wsl --install

# 列出可安装的有效 Linux 发行版
wsl --list --online

# 指定 Linux 发行版安装, 可用上面命令查看可安装的 Linux 发行版
wsl --install -d <Linux发行版名称>

# 取消注册指定 Linux 发行版，取消后可通过安装命令重新安装
wsl --unregister <Linux发行版名称>
```

### 默认设置

```shell
# 设置 WSL 版本，1 或 2，对应 WSL1 和 WSL 2
wsl --set-version <Linux发行版名称> <1或2>

# 设置默认 WSL 版本，1 或 2，对应 WSL1 和 WSL 2
wsl --set-default-version <1或2>

# 设置默认 Linux 发行版
wsl --set-default <Linux发行版名称>

# 设置 Linux 发行版的默认用户
<Linux发行版名称> config --default-user <用户名>
```

### 启动与停止

```shell
# 启动默认 Linux 分发版
wsl
 
# 以指定用户身份运行 Linux 分发版
wsl --user <用户名>
 
# 运行指定的 Linux 分发版
wsl -distribution <Linux分发版名称>
 
# 退出 Linux 分发版
exit
 
# 终止指定的 Linux 发行版
wsl --terminate <Linux分发版名称>
 
# 立即终止所有正在运行的 Linux 发行版及 WSL 轻型工具虚拟机
wsl --shutdown
```

### 导入与导出

此命令可用于迁移 WSL 至非系统盘符。如需迁移请参考: [迁移安装位置](#迁移安装位置)

```shell
# 将指定的 tar 文件导入 Linux 发行版, 可加 --version 指定要用于新发行版的版本
wsl --import <Linux分发版名称> <安装位置> <文件名>

# 将指定的 .vhdx 文件导入为新 Linux 发行版 
wsl --import-in-place <Linux分发版名称> <文件名>
 
# 导出 Linux 发行版，后缀名以 tar 结尾
wsl --export <Linux分发版名称> <文件名>
```

### 挂载与卸载

```shell
# 挂载物理磁盘 
wsl --mount <文件路径> 

# 挂载虚拟磁盘
wsl --mount <文件路径> --vhd

# 卸载指定磁盘
wsl --unmount <文件路径>

# 卸载所有挂载磁盘
wsl --unmount  

```

### 列出发行版

```shell
# 列出已安装且可用的 Linux 发行版
wsl --list 

# 列出可安装的有效 Linux 发行版
wsl --list --online 

# 列出已安装的 Linux 发行版
wsl --list --verbose

# 列出所有 Linux 发行版
wsl --list --all

# 列出正在运行的 Linux 发行版
wsl --list --running

# 仅显示 Linux 发行版名称
wsl --list --quiet 
```

### 其他命令

```shell
# 更新 WSL
wsl --update

# 检查 WSL 状态
wsl --status

# 检查 WSL 版本
wsl --version

# help 命令，可以查看 WSL 可用的选项和命令列表
wsl --help

# 打开 WSL 调试 shell 以进行诊断
wsl --debug-shell

# 打开 Windows 事件查看器的应用视图
wsl --event-viewer
```

## 迁移安装位置

> 对于一个轻度强迫症的技术宅来说，完全不能容忍软件安装在系统盘，所以我们可以把子系统迁移到到其他位置。

### 导出 tar 文件

将 Linux 发行版到处到想迁移的位置，注意导出为以 .tar 结尾的文件路径。

```shell
# 示例 : 导出 Linux 发行版 Ubuntu 到 D:\SubLinux 下，文件为 Ubuntu.tar 
# 注意 : 如文件路径有空格，参数需包裹在双引号内 如 "D:\Sub Linux\Ubuntu.tar"
wsl --export Ubuntu  D:\SubLinux\Ubuntu.tar
```

### 卸载当前 Linux 发行版

```shell
# 命令格式: wsl --unregister <Linux发行版名称>
wsl --unregister Ubuntu
```

### 导入已迁移的 tar 文件

导入上面导出到 `D:\SubLinux\Ubuntu.tar` 的文件，至此迁移就完成了。

```shell
# 命令格式: wsl --import <Linux发行版名称> <安装位置> <文件路径>
wsl --import Ubuntu "D:\SubLinux" "D:\SubLinux\Ubuntu.tar"
```

## 设置开机自启

::: warning 友情提示
WSL 默认不会随 Windows 启动，如果 WSL 服务可以随 Windows 系统启动，这将对在 WSL 下搭建的自启服务或者环境很有用。
:::

- ### 打开 Windows 启动菜单

快捷键 `win` + `R` 打开 运行窗口，输入 `shell:startup` 然后确定。

![image-20230604223842341](https://gitee.com/x0e/assets/raw/master/blog/image-20230604223842341.png)

会跳转至 Windows 启动菜单目录 , 具体路径如下:

`C:\Users\用户名\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup`

- ### 创建自启脚本

新建文本文档，将如下命令复制进去，文件名称可以随便起，然后另存为 .vbs 格式，自启文件就配置好了，后面 WSL 服务就会随着 Windows
自启了。

```shell
Set ws = WScript.CreateObject("WScript.Shell")
ws.run "wsl",vbhide
```
