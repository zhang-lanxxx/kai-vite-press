---
title: Windows 系统
index: 1
editLink: false
lastUpdated: false
next: false
---

::: tip 摘要

整理了部分 Windows 系统使用封装会涉及到的操作，点击链接可快速跳转至对应内容 :

- [Windows 纯净版系统镜像下载](download)
- [Windows 系统镜像精简](reduce)
- [Windows 系统镜像优化](optimize)
- [常用软件静默安装参数](silent)
  :::


