---
title: 静默安装
index: 5
---

## 前言

> 想必你也和之前我一样的经历，每次装完系统要花上半天时间装一大堆软件环境，而且有的软件默认还会安装到系统盘，很是令人头疼，其实安装包也是支持命令行参数的，并且可以通过静默参数实现无人值守安装，这样我们就可以通过封装命令脚本，真正的实现一键自动部署环境。

## 常见安装包类型

> 提到了命令行参数，肯定得先了解程序安装包的打包方式，因为不同的打包方式默认提供的安装参数是不同的。

### Microsoft Windows Installer

> Microsoft Windows Installer 是 Windows 提供的安装和配置服务。 Windows Installer 为安装和卸载软件提供标准基础。
> 使得软件制造商可以快捷轻松地进行软件安装、维护和卸载。

::: warning 提示
从 Windows Installer 3.0 开始，可以使用 Msiexec 命令行选项。 Windows Installer 3.0 及更早版本提供 Windows Installer 命令行选项
:::

```shell
# 命令格式
msiexec /Option <Required Parameter> [Optional Parameter]
```

#### [Windows Installer 命令行选项](https://learn.microsoft.com/zh-cn/windows/win32/msi/command-line-options)

| 命令选项                                   | 命令描述                                                                       |
|----------------------------------------|----------------------------------------------------------------------------|
| /i                                     | 安装或配置产品                                                                    |
| /a                                     | 管理安装 - 在网络上安装产品                                                            |
| /j <u \| m> <Product.msi>  [/t ] [/g ] | 公布产品<br />m 公布到所有用户<br />u 公布到当前用户                                         |
| /x <Product.msi>                       | 卸载产品                                                                       |
| /q [ n \| b \| r \| f ]                | 设置用户界面级别<br/>		n - 无用户界面<br/>		b - 基本界面<br/>		r - 精简界面<br/>		f - 完整界面(默认值) |
| /?                                     | 帮助信息                                                                       |

#### [Microsoft 标准安装程序命令行选项](https://learn.microsoft.com/zh-cn/windows/win32/msi/standard-installer-command-line-options)

| 命令选项                            | 命令描述            |
|:--------------------------------|:----------------|
| /help                           | 帮助信息            |
| /quiet                          | 安静模式，无用户交互      |
| /passive                        | 无人参与模式 - 只显示进度栏 |
| /norestart                      | 安装后不重新启动        |
| /forcerestart                   | 安装后始终重新启动计算机    |
| /promptrestart                  | 必要时提示用户重新启动     |
| /x <Product.msi \| ProductCode> | 卸载产品            |
| /package <Product.msi>          | 安装或配置产品         |
| /update                         | 更新程序组件          |

### InstallShield

> Installshield 是目前较为领先的用于 Windows
> 软件安装包开发的制作工具，能够帮助所有规模的软件生产商创建高质量的安装，以传统 MSI
> 方式和虚拟格式，自动化地封装，捆绑和包装产品，并使其可以配置数据库服务器、Web 服务以及移动设备等，实用性非常高。

参考自 [Revenera InstallShield](https://www.revenera.com/install/products/installshield)

| 命令选项 | 命令描述 |
|------|------|
|      |      |
|      |      |
|      |      |
|      |      |
|      |      |
|      |      |
|      |      |
|      |      |
|      |      |

- ### Wise Installation System

> Wise Installation System，是一个易用的、基于脚本的安装工具，在 Windows 平台下，它很大范围内满足了有经验的软件开发者的高级安装需求，包括
> Web 配置和自修复应用。


参考自 [WISE installer command line options](https://wpkg.org/index.php?title=WISE_installer)

| 命令选项 | 命令描述            |
|------|-----------------|
| /T   | 测试模式            |
| /X   | 提取文件到指定路径       |
| /Z   | 提取文件到指定路径并重启系统  |
| /S   | 静默安装 / 卸载       |
| /M   |                 |
| /M1  |                 |
| /M2  |                 |
| /A   | 卸载参数，自动模式，仅展示取消 |
| /R   | 卸载参数，回滚模式       |
| /U   | 卸载参数，           |
|      |                 |
|      |                 |

- ### Inno Setup

> Inno Setup。它是一款 Windows 免费的安装制作软件，功能强大，制作快速，向导丰富，上手容易。对于一般的 Windows 安装制作都能够快速地完成，并且
> Inno Setup 软件小巧、操作简便、界面精美，功能齐全，同样支持脚本批处理，受到了很多用户的喜爱。

其他支持命令可参考 [Setup Command Line Parameters](https://jrsoftware.org/ishelp/topic_setupcmdline.htm)

| 命令选项              | 命令描述                     |
|-------------------|--------------------------|
| /HELP 或 /?        | 帮助信息                     |
| /SP-              | 禁用安装询问                   |
| /SILENT           | 静默安装，但安装报错会提示，只展示进度条     |
| /VERYSILENT       | 静默安装，安装报错不提示，需要重启会自动重启系统 |
| /SUPPRESSMSGBOXES | 禁用提示消息框                  |
| /ALLUSERS         | 以管理安装模式安装                |
| /CURRENTUSER      | 以非管理员模式安装                |
| /NORESTART        | 安装后不重新启动                 |
| /NOCANCEL         | 禁止安装过程中取消                |
| /DIR              | 指定安装目录                   |

- ### Nullsoft Scriptable Install System (NSIS)

> NSIS（Nullsoft Scriptable Install System）是一个专业的开源系统，它提供了安装、卸载、系统设置、文件解压缩等功能，非常适合用于创建
> Windows 安装程序。

参考自 [Installer Usage](https://nsis.sourceforge.io/Docs/Chapter3.html#installerusage)

| 命令选项  | 命令描述                              |
|:------|:----------------------------------|
| /S    | 静默安装 / 卸载                         |
| /NCRC | 跳过安装程序的CRC检查                      |
| /D    | 设置默认安装目录，必须是命令行最后一个参数，并且不能包含任何引号。 |



## 常见软件静默参数

::: warning 提示
示例命令中静默安装参数 INSTALL_PATH 均为安装路径，使用时按需替换即可
:::

## 开发相关软件

### Git

```shell
# 安装路径 INSTALL_PATH
/DIR='INSTALL_PATH' /VERYSILENT /NORESTART /SP- /SUPPRESSMSGBOXES

# 示例
/DIR='D:\Develop Software\Git' /VERYSILENT /NORESTART /SP- /SUPPRESSMSGBOXES
```

### SourceTree

```shell
# 解压SourceTree安装包到指定安装路径
xcopy ".\Files\PostmanTemp" "D:\Develop Software\PostmanTemp" /E /Y /D
# 进入"D:\Develop Software\PostmanTemp"执行命令
./Update.exe --install .
# 关闭安装窗口
taskkill.exe /F /IM Postman.exe
# 移除目录
rmdir /s/q "D:\Develop Software\PostmanTemp"
```

### Postman

```shell
# 解压SourceTree安装包到指定安装路径
xcopy ".\Files\SourceTreeTemp" "D:\Develop Software\SourceTreeTemp" /E /Y /D
# 进入"D:\Develop Software\SourceTreeTemp"执行命令
./Update.exe --install .
# 关闭安装窗口
taskkill.exe /F /IM SourceTree.exe
# 移除目录
rmdir /s/q "D:\Develop Software\SourceTreeTemp"
```

### JetBrains 全家桶

::: warning 提示
JetBrains 系列软件均可通过此静默参数指定目录安装，这里只针对常用的 IDEA 及 Webstorm 进行示例说明
:::

#### Intellij IDEA

> 参考自 [Silent installation on Windows](https://www.jetbrains.com/help/idea/installation-guide.html#silent)

```shell
# 静默指定目录安装
/S /D=INSTALL_PATH

# 示例
/S /D=D:\Develop Software\Intellij IDEA    
```

#### WebStorm

> 参考自 [Silent installation on Windows](https://www.jetbrains.com/help/webstorm/installation-guide.html#silent)

```shell
# 静默指定目录安装
/S /D=INSTALL_PATH

# 示例
/S /D=D:\Develop Software\Webstorm
```

### Typora

```shell
# 静默指定目录安装
/DIR='INSTALL_PATH' /ALLUSERS /VERYSILENT /SP- /SUPPRESSMSGBOXES

# 示例 
/DIR='D:\General Software\Typora' /ALLUSERS /VERYSILENT /SP- /SUPPRESSMSGBOXES 
# 关闭进程
taskkill.exe /F /IM typora.exe
```

### NotePad++

```shell
# 静默指定目录安装
/S /D=INSTALL_PATH

# 示例
/S /D=D:\General Software\NotePad++
```

### JDK 1.8

>
参考自 [JDK Installation for Microsoft Windows](https://docs.oracle.com/javase/8/docs/technotes/guides/install/windows_jdk_install.html)

```shell
# 静默指定目录安装
/s /lang=2052 ADDLOCAL="ToolsFeature,SourceFeature" INSTALLDIR="INSTALL_PATH" WEB_JAVA=0 AUTO_UPDATE=0 NOSTARTMENU=1

# 示例
/s /lang=2052 ADDLOCAL="ToolsFeature,SourceFeature" INSTALLDIR="D:\Develop Software\Java\JDK" WEB_JAVA=0 AUTO_UPDATE=0 NOSTARTMENU=1
```

### JDK 11

>
参考自 [JDK Installation Guide](https://docs.oracle.com/en/java/javase/11/install/installation-jdk-configuration-file.html)

```shell
# 静默指定目录安装
INSTALLDIR="INSTALL_PATH" INSTALL_SILENT=Enable

# 示例
INSTALLDIR="D:\Develop Software\OpenJDK" INSTALL_SILENT=Enable
```

### Node.js

```shell
# 静默指定目录安装
/passive INSTALLDIR="INSTALL_PATH"

# 示例
/passive INSTALLDIR="D:\Develop Software\Node.js"
```

### uTools

```shell
# 静默指定目录安装
/S /D="INSTALL_PATH"

# 示例
/S /D="D:\General Software\uTools"
```

### XMind

```shell
# 静默指定目录安装
/S /D="INSTALL_PATH"

# 示例
/S /D="D:\General Software\XMind"
```

### SQLyog

>
参考自 [Silent installation and registration](https://sqlyogkb.webyog.com/article/43-silent-installation-and-registration)

```shell
# 静默指定目录安装
/DIR="INSTALL_PATH" /VERYSILENT /SP-

# 示例
/DIR="D:\Develop Software\SQLyog" /VERYSILENT /SP-
```

### Redis Desktop Manager(RESP.app)

::: warning 提示
因 RESP 依赖于 VisualC++ 库运行，所以需要安装 VC_redist.x64 才能运行
:::

```shell
# VC_redist.x64 依赖安装
/install /quiet

# 静默指定目录安装
/S /D=INSTALL_PATH

# 示例
/S /D=D:\Develop Software\RESP

# 杀死自启进程
taskkill.exe /F /IM typora.exe
```

## 系统相关软件

### 数字权利激活程序

```shell
/Q
```

### 搜狗输入法

```shell
# 静默指定目录安装
/S /D=INSTALL_PATH

# 示例
/S /D=D:\General Software\Sougou

```

### 360 极速 X 浏览器

> 参考自 [360极速浏览器静默安装到指定文件夹](https://bbs.360.cn/thread-16031878-1-1.html)

```shell
# 静默指定目录安装
--silent-install=3_1_1 --install-path="INSTALL_PATH"

# 示例
--silent-install=3_1_1 --install-path="D:\General Software\360ChromeX"
```

### 360 zip

```shell

```

### 钉钉

### 微信

###                                               
