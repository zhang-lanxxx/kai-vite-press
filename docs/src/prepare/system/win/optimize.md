---
title: 系统优化
index: 2
---

## 准备工作

- Windows 镜像 : 获取方式可参考 [镜像下载](Windows系统镜像下载)
- 优化工具 :  [Dism++](https://github.com/Chuyu-Team/Dism-Multi-language/releases)

::: tip 提示
为保证教程可用性，尽量使用微软原版 Windows 镜像，目前主流 Intel / AMD 处理器均不支持 x86
架构系统，所以仅针对 x64 架构进行示例说明。
:::

## Dism++ 介绍

Dism++ 是一款免费且强大的 Windows 系统管理优化工具，基于 Dism.exe 命令行工具封装，可以自定义设置和优化。相当于一个“一站式”管理优化工具集。

- 兼容 Vista、Win7、Win10、Windows 11 等高低版本系统。
- 拥有全面深度的垃圾清理功能。
- 开机启动项管理，包括自启软件和系统服务。
- Appx 管理，支持批量删除预装应用。
- 提供备份还原、引导修复、账户管理以及镜像格式转换等工具。
- 系统优化设置、驱动管理、Windows 功能管理、可选功能管理、驱动及更新管理等。

## 工具使用

### 挂载镜像

- 鼠标双击 ISO 镜像文件，将系统镜像挂载到驱动器，会看到如下目录结构。

![image-20230530101833684](https://gitee.com/x0e/assets/raw/master/blog/image-20230530101833684.png)

- 将驱动器内的文件全部拷贝到一个空的文件夹，尽量避免拷贝路径有空格。

![image-20230530105418272](https://gitee.com/x0e/assets/raw/master/blog/image-20230530105418272.png)

- 拷贝完成后，右键以管理员身份运行 Dism++x64.exe ，第一次打开会有弹窗，点击 `接受` ，会进入如下界面。

![image-20230530110623895](https://gitee.com/x0e/assets/raw/master/blog/image-20230530110623895.png)

::: warning 提示
Dism++ 会自动挂载当前操作系统，操作时注意区分是否在当前挂载镜像会话 ！！！
:::

- 点击左上角菜单，`文件` ➡️ `挂载映像`

![image-20230530110957278](https://gitee.com/x0e/assets/raw/master/blog/image-20230530110957278.png)

- 弹出如下页面，选择拷贝目录下的 `\sources\install.wim` 文件，`目标映像`
  就会自动识别出系统版本，可按需自行选择其他版本，这里选择要挂载的 `Windows 11专业版`
  ，然后再指定挂载目录，这里选择挂载到 `E:\Target`
  目录下，点击 `确认` 后会对镜像进行挂载。

![image-20230530112110181](https://gitee.com/x0e/assets/raw/master/blog/image-20230530112110181.png)

- 耐心等待直至状态变为 `准备就绪`，说明挂载完成，然后点击 `打开会话`。

![image-20230530120912368](https://gitee.com/x0e/assets/raw/master/blog/image-20230530120912368.png)

- 会打开如下页面，接下来就可以进行下一步优化了。

![image-20230530121121680](https://gitee.com/x0e/assets/raw/master/blog/image-20230530121121680.png)

### 系统文件清理

::: warning 提示
此处清理文件，不会影响系统稳定性，若需要保留 `系统自带 Appx 应用` ，取消勾选即可。
:::

- 按需选择清理项，点击 `扫描` ，等待扫描完成后点击 `清理` ，若出现弹窗，点击 `确认`。

![image-20230530122058672](https://gitee.com/x0e/assets/raw/master/blog/image-20230530122058672.png)

### 启动项管理

- 开机启动项禁用

![image-20230530123943739](https://gitee.com/x0e/assets/raw/master/blog/image-20230530123943739.png)

### Appx 管理

- 删除 Windows 内置 Appx 应用，按需勾选删除的应用，点击 `删除` 即可，参考保留如下：

::: tip 提示
非平板电脑使用，均可删除，可根据使用需求自行选择。
:::

![image-20230530125801257](https://gitee.com/x0e/assets/raw/master/blog/image-20230530125801257.png)

### 系统优化

- 任务栏相关设置

::: warning 注意
开启 `隐藏操作中心任务栏托盘` ，会导致控制面板无法打开。
:::

![image-20230530131654446](https://gitee.com/x0e/assets/raw/master/blog/image-20230530131654446.png)

- Windows 主题相关设置

![image-20230530131953091](https://gitee.com/x0e/assets/raw/master/blog/image-20230530131953091.png)

- 安全相关设置

![image-20230530132206492](https://gitee.com/x0e/assets/raw/master/blog/image-20230530132206492.png)

- 开始菜单以及 Windows 体验

![image-20230530133109924](https://gitee.com/x0e/assets/raw/master/blog/image-20230530133109924.png)

- Explorer 资源管理器

![image-20230530133242240](https://gitee.com/x0e/assets/raw/master/blog/image-20230530133242240.png)

- 桌面图标管理

![image-20230530133452365](https://gitee.com/x0e/assets/raw/master/blog/image-20230530133452365.png)

- 资源管理器导航窗口图标管理

![image-20230530133540076](https://gitee.com/x0e/assets/raw/master/blog/image-20230530133540076.png)

- 右键菜单管理

![image-20230530133712380](https://gitee.com/x0e/assets/raw/master/blog/image-20230530133712380.png)

- MicoSoft Edge

![image-20230530133941245](https://gitee.com/x0e/assets/raw/master/blog/image-20230530133941245.png)

- Internet Explorer

![image-20230530134030663](https://gitee.com/x0e/assets/raw/master/blog/image-20230530134030663.png)

- 微软拼音输入法

![image-20230530134105807](https://gitee.com/x0e/assets/raw/master/blog/image-20230530134105807.png)

- Windows Update

![image-20230530134142659](https://gitee.com/x0e/assets/raw/master/blog/image-20230530134142659.png)

- 记事本

![image-20230530134235195](https://gitee.com/x0e/assets/raw/master/blog/image-20230530134235195.png)

- 网络设置

![image-20230530134352178](https://gitee.com/x0e/assets/raw/master/blog/image-20230530134352178.png)

- 服务优化

![image-20230530134511042](https://gitee.com/x0e/assets/raw/master/blog/image-20230530134511042.png)

- Windows Media Player

![image-20230530134927712](https://gitee.com/x0e/assets/raw/master/blog/image-20230530134927712.png)

- Windows 照片查看器

![image-20230530134849554](https://gitee.com/x0e/assets/raw/master/blog/image-20230530134849554.png)

- 其他

::: warning 提示
笔记本不建议关闭 `快速启动`和`休眠`
:::

![image-20230530134805869](https://gitee.com/x0e/assets/raw/master/blog/image-20230530134805869.png)

### 保存镜像

- 如上步骤优化完成后，点击左上角菜单，`文件` ➡️ `保存映像`

![image-20230530135448631](https://gitee.com/x0e/assets/raw/master/blog/image-20230530135448631.png)

- 会有弹窗提醒 `你需要增量保存还是直接保存？`，选择 `直接保存`

![image-20230530135831323](https://gitee.com/x0e/assets/raw/master/blog/image-20230530135831323.png)

### 卸载镜像

- 保存完成后，点击左上角菜单，`文件` ➡️ `卸载映像`，耐心等待卸载完成，即可关闭 Dism++ ，至此 Windows 系统镜像优化就完成了。

![image-20230530140208470](https://gitee.com/x0e/assets/raw/master/blog/image-20230530140208470.png){width=50% #fig:image-20230530140208470}

<style> 
img { 
  float: center;
  margin-right: auto;
  margin-left: auto;
}
</style>    
