---
title: 镜像下载
index: 1
---

::: tip 提示
为保证系统干净，没有其他污染，尽可能下载官方纯净版 `ISO`/`ESD`
镜像，这里推荐三种官方原版镜像获取途径，请根据情况自行选择，确保系统镜像完整性，下载完成后务必对文件进行 `MD5` 和 `SHA1`
校验比对。
:::

## 镜像获取方式

### ① itellyou(我告诉你)

访问地址: [https://next.itellyou.cn/](https://next.itellyou.cn/)

> 非微软官方，国内一个原版信息收录站点，07 年至今一直秉持着提供原版软件获取方法的初衷，是比较可靠的原版镜像资源获取途径。同时站点下载页面也提供了对应的文件校验信息，方便我们比对镜像文件是否被篡改。

::: warning 提示
此方式仅提供官方 ED2K 和 BT 链接，不支持直链下载，需要借助额外下载工具。
:::

### ② Windows 更新助手

下载地址: [https://www.microsoft.com/zh-cn/software-download](https://www.microsoft.com/zh-cn/software-download)

> 微软官方提供的系统升级工具。

- 支持系统在线升级至最新版本。
- 支持创建系统安装介质到 USB。
- 支持下载系统镜像 ISO 文件。

::: warning 提示
更新助手下载的镜像文件为有更高压缩率的 ESD 格式，本质与 ISO 格式无异，此教程均可使用，缺点是仅可下载最新版本系统镜像。
:::

### ③ UUP dump <Badge type="tip" text="推荐" />

访问地址 : [https://uupdump.net/?lang=zh-cn](https://uupdump.net/?lang=zh-cn)

国内镜像地址 : [https://www.uupdump.cn/](https://www.uupdump.cn/)

> UUP dump 是一个基于微软 2016 年发布的统一升级平台 ( Unified Update Platform )
> 实现的系统镜像下载平台，可以根据用户选择的系统版本、语言、下载方式、转换选择等，来生成对应的命令脚本压缩包，通过运行对应平台脚本一键下载
> UUP 集，并使用内置脚本自动转换成 ISO / ESD 文件。

- 支持系统版本关键字及内部 `build` 版本号搜索。
- 采用 `Aria2` 下载器，支持多线程。
- 支持按需选择 `Windows Apps` 及系统更新是否构建到系统镜像。
- 仅支持 `Windows 8.1`、 `Windows10`、`Windows11` 正式版及预览版系统镜像下载。
- 脚本支持 `Windows`、`Linux`、`macOS` 等平台运行。

## 获取镜像更新

> 这里以 UUP dump 方式为例，来下载最新的 Windows 11 正式版系统镜像。
>

### UUP dump 官网说明

> 打开 [UUP dump 官网](https://uupdump.net/?lang=zh-cn)，会看到如下页面 :

![UUP dump 首页](https://gitee.com/x0e/assets/raw/master/blog/25e9107a-8c3d-4357-9df9-9a4a8fc8b01a.png)

::: tip 快速选项说明

- 发布类型
  - 公开发布的最新内部版本 即 `正式版系统`。
  - 想尝鲜未公开发布系统功能可以选择如下通道版本，但不推荐作为主力系统使用，按照顺序排名越靠后，系统稳定性越差。
    - 最新发布预览通道版本
    - 最新 Beta 通道版本
    - 最新 Dev 通道版本
    - 最新 Canary 渠道版本
- 体系结构
  - x64 - 近代主流 `Intel` / `AMD` 系列处理器均采用此架构。
  - arm64 - 主流大部分手机处理器及 `Apple M系列` 均采用 `ARM` 架构。

:::

### 获取最新版本

- 以 `正式版本 x64` 架构为例，点击发布类型 `公开发布的最新内部版本` 右侧的 `x64` 体系结构按钮

![UUP dump 首页](https://gitee.com/x0e/assets/raw/master/blog/25e9107a-8c3d-4357-9df9-9a4a8fc8b01a.png)

- 会跳转到如下列表，点击对应链接，然后进行下一步 [下载脚本](#下载脚本)

![image-20230529155125156](https://gitee.com/x0e/assets/raw/master/blog/image-20230529155125156.png)

### 版本号检索

> 如果下载最新版本可 [跳过此步骤](#下载脚本)，若不想下载最新版本，可以根据指定 `build` 内部版本号进行检索，具体 `build`
> 内部版本号请参考 :
> <br/>[Windows 11 版本信息](https://learn.microsoft.com/zh-cn/windows/release-health/windows11-release-information)
> | [Windows 10 发布信息](https://learn.microsoft.com/zh-cn/windows/release-health/release-information)

- 以 Windows 11 22H2 为例，最新内部版本为 22621.1702

![image-20230529152845215](https://gitee.com/x0e/assets/raw/master/blog/image-20230529152845215.png)

- 根据内部版本号 22621.1702 搜索

![image-20230529153429699](https://gitee.com/x0e/assets/raw/master/blog/image-20230529153429699.png)

- 出现如下列表，点击对应更新版本链接，然后进行下一步 [下载脚本](#下载脚本) ，注意体系结构是否正确。

![image-20230529153750224](https://gitee.com/x0e/assets/raw/master/blog/image-20230529153750224.png)

## 下载脚本

::: tip 提示
如上两种方式 ，获取到更新版本列表后，选择对应的版本，会跳转至语言选择界面。
:::

### 选择语言

- 默认为中文(简体)，可根据自己需求自行选择，然后点击下一步。

![image-20230529161854829](https://gitee.com/x0e/assets/raw/master/blog/image-20230529161854829.png)

### 选择虚拟升级版本

- 这里只用到专业版，只选择 `Windows Pro`即可 ，可按照需求自己选择，然后点击下一步。

![image-20230529162139313](https://gitee.com/x0e/assets/raw/master/blog/image-20230529162139313.png)

### 选择下载及转换选项

- 这里可以按需勾选，然后点击创建下载包。

![image-20230529162706897](https://gitee.com/x0e/assets/raw/master/blog/image-20230529162706897.png)

## 解压压缩包

- 压缩包下载完成后，解压到磁盘目录，文件结构如下 :

::: danger 注意
压缩包解压路径不能包含空格，否则脚本执行会报错 !
:::

![image-20230529163452009](https://gitee.com/x0e/assets/raw/master/blog/image-20230529163452009.png)

## 下载镜像

- 按照系统运行对应脚本，以`Windows` 系统为例，鼠标右键 `uup_download_windows.cmd` 以 `管理员身份运行`
  会打开如下窗口，不要关闭窗口，耐心等待执行完成。

::: warning 提示
自定义构建可参考脚本目录下 ReadMe.html 文件，这里不展开说明。
:::

![image-20230529163943721](https://gitee.com/x0e/assets/raw/master/blog/image-20230529163943721.png)

- 脚本命令执行完成页面如下，按 `0` 退出命令窗口即可。

![image-20230529173500150](https://gitee.com/x0e/assets/raw/master/blog/image-20230529173500150.png)

- 此时在脚本目录下会生成如下镜像文件，至此纯净版系统镜像就下载完成了。

![image-20230529174132246](https://gitee.com/x0e/assets/raw/master/blog/image-20230529174132246.png)

