---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  #  name: "nil "
  text: "记录文档"
  tagline: Recording...
  actions:
    - theme: brand
      text: 快速开始
      link: /prepare/system/win/
    - theme: alt
      text: 全部标签
      link: /base/tags

features:
  - title: Environment & Tool
    details: · 操作系统优化 <br/> · 开发环境搭建 <br/> · 必备工具推荐 <br/> · 常用脚本命令  <br/>
    link: /prepare/system/win/download
    linkText: 了解更多
  - title: Java 后端知识整理
    details: · Java SE 基础知识 <br/> · Java EE 核心技术 <br/> · Java 技术进阶 <br/> · 主流技术框架学习
    link: /backend/
    linkText: 了解更多
  - title: 前端知识学习整理
    details: · HTML CSS JavaScript 基础知识 <br/> · ES6 语法整理 <br/> · TypeScript 基础学习 <br/> · Vue.js 学习
    link: /frontend/
    linkText: 了解更多
---

<style>
/*爱的魔力转圈圈*/
.m-home-layout .image-src:hover {
  transform: translate(-50%, -50%) rotate(666turn);
  transition: transform 59s 1s cubic-bezier(0.3, 0, 0.8, 1);
}

.m-home-layout .details small {
  opacity: 0.8;
}

.m-home-layout .item:last-child .details {
  display: flex;
  justify-content: flex-end;
  align-items: end;
}
</style>
