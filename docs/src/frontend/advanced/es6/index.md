---
title: ES6 基础语法
index: 0
---

:::info
哈哈哈哈
:::

## 认识ES6

> `ECMA`是 "European Computer Manufacturers Association" 的缩写,中文称欧洲计算机制造联合会,这个组织的目标是评估、开发和认可电信和计算机标准
>
> `ECMA`是标准, `JavaScript`是实现
>
> 目前高级浏览器支持`ES6`,低级浏览器主要支持`ES3.1`

## let 和 const

- 代码示例

  ```js
  // 1. var可以重复声明 
  // 2. var无法限制修改,var没有块级作用域,只有在 function 中有效
  var num = 10;
  num = 20;
  if(true){
    num = 30  
    console.log(num)  
  }
  console.log(num)
  // 闭包
  (function(i){})(i)
  -------------------------------------------------
  // let 声明的变量,只在let命令所在的代码块内有效
  // let 不存在变量提升
  // let 不允许在相同作用域内,重复声明同一个变量
  
  // const 声明一个只读的常量,一旦声明,常量的值就不能改变
  // const 声明的常量不能改变值,且声明时必须初始化    
  // const 声明的常量,只在声明所在的块级作用域内有效
  // const 声明的常量不提升,只能在声明位置后使用
  // const 声明的常量与 let 一样不可重复声明
  // const 声明的复合类型数据,变量指向的内存地址    
  ```

#### 3 . 箭头函数

> 箭头函数使得表达更加简洁
>
> 箭头函数能够简化回调函数

- 使用示例

  ```js
  // 普通函数
  const res = function(x){return x * x;}
  
  // 如果没有参数或者多个参数需要使用 () 来定义参数列表
  const res = () => {console.log()};
  const res = (a,b) => {console.log(a,b)};
  // 如果只有一个参数,可以不用()
  const res = x => {console.log(x)};
  // 如果函数体只有一条语句,可以不用 {} , 也不用使用 return 
  const res = x => console.log(x);
  
  // 箭头在返回对象时,必须在对象外边加上 ()
  const obj = id => ({return{id:id.name:'张三'}})
  ```

- `this指向`

  ```js
  // 普通函数的this: 指向它的调用者,没有调用者默认指向window
  // 箭头函数的this: 
  //	指向箭头函数定义时所处对象,而不是箭头函数使用时所在的对象,默认使用父级的this
  
  // 箭头函数没有自己的this,它的this是继承而来,默认指向在定义它时所处的对象(宿主对象)
  ```

#### 4 . 数组的新增方法

- 使用示例

  ```js
  // 定义数组
  let goods = [30,45,50,5,19,65,82]
  // 遍历数组
  for(let n of goods){
      console.log(n)
  }
  
  // filter  过滤器: 过滤符合条件的数据
  let res = goods.filter(n >= 1);
  // map 映射: 处理并组装数据
  let res = goods.map((n) => n * 0.5)
  // reduce 汇总: 累加上次执行的结果
  let sum = goods.reduce((s,n)=> s + n ,0)
  // some() 
  // every()
  
  // 链式调用
  let res = goods.filter(n >= 1).map((n) => n * 0.5).reduce((s,n)=> s + n ,0)
  ```

#### 5 . `Set`和`Map`的数据结构

- 使用示例

  ```js
  let obj = new Map()
  
  // 添加 
  obj.set('key','value')
  // 获取 
  obj.get('key')
  // 删除 
  obj.delete('key')
  // 判断是否存在
  obj.has('key')
  // 清空
  obj.clear()
  // 获取长度
  obj.size
  
  // 遍历
  keys() // 返回所有键名的遍历器
  values() // 返回所有键值的遍历器
  entries() // 返回所有成员的遍历器
  forEach() // 遍历Map的所有成员
  ```

#### 6 . 字符串新增功能

- 示例

  ```js
  // 定义字符串
  let str = "ABCDEF";
  
  // 判断以什么字符串开头
  str.startswith('AB')
  // 判断以什么字符串结尾
  str.endsWith('EF')
  // 模板字符串
  let template = `		
  				<div>
  					<h1>${str}</h1>
  				</div>	
  				`
  ```

#### 7 . 解构赋值和扩展运算符

- 使用示例

  ```js
  // 定义数组
  let arr = [1,2,3];
  // 定义对象
  let obj = {name:'张三',age:18,sex:'男'};
  
  // 解构赋值
  let [a,b,c] = arr;
  let {name,age,sex} = obj;
  
  // 扩展运算符
  let arr2 = [7,8,9];
  let res = [...arr,arr2]
  
  // 方法调用时使用
  function show(a,b,c){
      console.log(a,b,c)
  }
  show(...arr);
  
  // 方法声明时使用
  function show(...arr){
      console.log(arr)
  }
  ```

#### 8 . 对象的新语法

- 使用示例

  ```js
  // 声明对象 
  class  Person{
      constructor(name,age,sex){
          this.name = name;
          this.age = age;
          this.sex = sex;
      }
      hello(){
      	console.log('hello')    
      }
  }
  
  // 继承
  class Student extends Person{
      constructor(name,age,sex,school){
          super(name,age,sex);
          this.school = school;
      }
  }
  
  
  // JSON 对象
  const json  = {name:'张三',age:10,sex:'男',hello(){console.log()}}
  
  // JSON 串行化
  let str = JSON.stringify(obj);
  // JSON 反串行化
  let obj = JSON.parse(str);
  ```

#### 9 .`Module`模块化

- 使用示例

  ```js
  // 引入
  import {add} from './one.js'
  // 命名冲突(声明别名)
  import {add as nul} from './one.js'
  // 别名引入
  import custom from './one.js'
  import * as custom from './one.js'
  
  
  // 导出声明
  export function add(x,y){
      return x + y;
  }
  // 一个模块中只能有一个缺省导出(export default)
  export default function(args){
      console.log(args)
  }
  ```
