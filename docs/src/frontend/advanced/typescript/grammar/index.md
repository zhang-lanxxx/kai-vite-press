---
title: 语法介绍
index: 2
---

##### ① 基本数据

> 基本语法

```js
let 变量名 : 数据类型 = 值
```

###### ⑴ 布尔值

> 最基本的数据类型就是简单的 `true/false`值

```ts
let isDone: boolean = false;
isDone = true;
// isDone = 2 // error
```

###### ⑵ 数字

> `TypeScript` 里的所有数字都是浮点数。 这些浮点数的类型是 `number`。 除了支持十进制和十六进制字面量，`TypeScript`
> 还支持 `ECMAScript 2015`中引入的二进制和八进制字面量。

```ts
let a1: number = 10 // 十进制
let a2: number = 0b1010  // 二进制
let a3: number = 0o12 // 八进制
let a4: number = 0xa // 十六进制
```

###### ⑶ 字符串

> 可以使用双引号（`"`）或单引号（`'`）表示字符串。

```ts
let name:string = 'tom'
name = 'jack'
// name = 12 // error
let age:number = 12
const info = `My name is ${name}, I am ${age} years old!`
```

###### ⑷ `undefined` 和 `null`

> 默认情况下 `null` 和 `undefined` 是所有类型的子类型。 就是说你可以把 `null` 和 `undefined` 赋值给 `number` 类型的变量

```ts
let u: undefined = undefined
let n: null = null
```

###### ⑸ 数组

> 有两种方式可以定义数组。 第一种，可以在`元素类型后面接上[]`，表示由此类型元素组成的一个数组

```ts
// 数组定义方式1
let arr1:number[] = [10,20,30]
// 数组定义方式2
let arr2:Array<number> =  [100,200,300]
```

###### ⑹ 元组 `Tuple`

> 元组类型允许表示一个已知元素数量和类型的数组，`各元素的类型不必相同`
>
> 但数据的类型位置和数据个数需和定义元组的时候的数据类型保持一致

```ts
let arr3 :[string,number,boolean] = ['初始值',100,true]
```

###### ⑺ 枚举

> `enum` 类型是对 JavaScript 标准数据类型的一个补充,枚举里面的每个数据值都可以叫做元素,每个元素都有自己的编号

```ts
enum Color {
  Red,
  Green,
  Blue
}

// 枚举数值默认从0开始依次递增
// 根据特定的名称得到对应的枚举数值
let myColor: Color = Color.Green  // 0
console.log(myColor, Color.Red, Color.Blue)

// 默认情况下，从 0 开始为元素编号。 你也可以手动的指定成员的数值。
enum Color {Red = 1, Green, Blue}
let c: Color = Color.Green

// 或者全部手动赋值
enum Color {Red = 1, Green = 2, Blue = 4}
let c: Color = Color.Green

// 枚举类型提供的一个便利是你可以由枚举的值得到它的名字
enum Color {Red = 1, Green, Blue}
let colorName: string = Color[2]

console.log(colorName)  // 'Green'
```

###### ⑻ `any`

> 在编程阶段还不清楚类型的变量指定一个类型
>
> 它允许你在编译时可选择地包含或移除类型检查

```ts
let notSure: any = 4
notSure = 'maybe a string'
notSure = false // 也可以是个 boolean

// 当一个数组中要存储多个数据,类型不确定,个数也不确定,可以使用any类型来定义数组
let list: any[] = [1, true, 'free']
list[1] = 100
```

###### ⑼ `void`

> 某种程度上来说，`void` 类型像是与 `any` 类型相反，它`表示没有任何类型`。 当一个函数没有返回值时使用

```ts
/* 表示没有任何类型, 一般用来说明函数的返回值不能是undefined和null之外的值 */
function showMsg(): void {
  console.log('showMsg()')
  // return undefined
  // return null
  // return 1 // error
}

// 声明一个 void 类型的变量没有什么大用，因为你只能为它赋予 undefined 和 null
let unusable: void = undefined
```

###### ⑽ `object`

> `object` 表示非原始类型，也就是除 `number`，`string`，`boolean`之外的类型。

```ts
function fn2(obj:object):object {
  console.log('fn2()', obj)
  return {}
  // return undefined
  // return null
}
console.log(fn2(new String('abc')))
// console.log(fn2('abc') // error
console.log(fn2(String))
```

###### ⑾ 联合类型

> 联合类型（`Union Types`）表示取值可以为多种类型中的一种

```ts
function toString2(x: number | string) : string {
  return x.toString()
}
```

###### ⑿ 类型断言

> 类型断言好比其它语言里的类型转换，但是不进行特殊的数据检查和解构。 它没有运行时的影响，只是在编译阶段起作用。
>
> 语法方式1:  <类型>变量名
>
> 语法方式2: 值 as 类型 (`tsx` 中只能用此种方式)

```ts
/* 需求: 定义一个函数得到一个字符串或者数值数据的长度 */
function getLength(x: number | string) {
  if ((<string>x).length) {
    return (x as string).length
  } else {
    return x.toString().length
  }
}
console.log(getLength('abcd'), getLength(1234))
```

###### ⒀ 类型推断

> `TS`会在没有明确的指定类型的时候推测出一个类型

```ts
/* 定义变量时赋值了, 推断为对应的类型 */
let b9 = 123 // number
// b9 = 'abc' // error

/* 定义变量时没有赋值, 推断为any类型 */
let b10  // any类型
b10 = 123
b10 = 'abc'
```

##### ② 接口

> `TypeScript` 的核心原则之一是对值所具有的结构进行类型检查。接口是对象的状态(属性)和行为(方法)的抽象(描述)

###### ⑴ 可选属性

> 只是在可选属性名字定义的后面加一个 `?` 符号。
>
> 可选属性的好处之一是可以对可能存在的属性进行预定义，好处之二是可以捕获引用了不存在的属性时的错误。

```ts
interface IPerson {
  id: number
  name: string
  age: number
  sex?: string // 可有可无
}
```

###### ⑵ 只读属性

> 一些对象属性只能在对象刚刚创建的时候修改其值。 你可以在属性名前用 `readonly` 来指定只读属性
>
> 最简单判断该用 `readonly` 还是 `const` 的方法是看要把它做为变量使用还是做为一个属性。 做为变量使用的话用 `const`
> ，若做为属性则使用 `readonly`

```ts
interface IPerson {
  readonly id: number   // 只读属性
  name: string
  age: number
  sex?: string
}
```

> 一旦赋值后再也不能被改变了。

```ts
const person2: IPerson = {
  id: 2,
  name: 'tom',
  age: 20,
  // sex: '男' // 可以没有
  // xxx: 12 // error 没有在接口中定义, 不能有
}
person2.id = 2 // error
```

###### ⑶ 函数类型

> 通过接口的方式为函数的类型来使用
>
> 为了使用接口表示函数类型，我们需要给接口定义一个调用签名。它就像是一个只有参数列表和返回值类型的函数定义。参数列表里的每个参数都需要名字和类型。

```ts
// 定义调用签名
interface SearchFunc {
  (source: string, subString: string): boolean
}

// 定义一个函数,该类型就是上面定义的接口函数
const mySearch: SearchFunc = function (source: string, sub: string): boolean {
  return source.search(sub) > -1
}

console.log(mySearch('abcd', 'bc'))
```

###### ⑷ 类类型

> 类的类型可以通过接口来实现

- 类实现接口

  ```ts
  // 定义接口 Alarm
  interface Alarm {
    alert(): any;
  }
  
  // 定义接口 Light
  interface Light {
    lightOn(): void;
    lightOff(): void;
  }
  
  // 定义一个类实现如上接口
  class Car implements Alarm {
    // 实现接口方法  
    alert() {
        console.log('Car alert');
    }
  }
  ```

- 一个类可以实现多个接口

  ```ts
  class Car2 implements Alarm, Light {
    alert() {
      console.log('Car alert');
    }
    lightOn() {
      console.log('Car light on');
    }
    lightOff() {
      console.log('Car light off');
    }
  }
  ```

- 接口继承接口

  > 和类一样，接口也可以相互继承。 这让我们能够从一个接口里复制成员到另一个接口里，可以更灵活地将接口分割到可重用的模块里

  ```ts
  interface LightableAlarm extends Alarm, Light {
  }
  ```

##### ③ 类

###### ⑴ 基本示例

```ts
/* 
类的基本定义与使用
*/

class Greeter {
  // 声明属性
  message: string

  // 构造方法
  constructor (message: string) {
    this.message = message
  }

  // 一般方法
  greet (): string {
    return 'Hello ' + this.message
  }
}

// 创建类的实例
const greeter = new Greeter('world')
// 调用实例的方法
console.log(greeter.greet())
```

###### ⑵ 继承

> 基于类的程序设计中一种最基本的模式是允许使用继承来扩展现有的类

```ts
class Animal {
  run (distance: number) {
    console.log(`Animal run ${distance}m`)
  }
}

class Dog extends Animal {
  cry () {
    console.log('wang! wang!')
  }
}

const dog = new Dog()
dog.cry() 
dog.run(100) // 可以调用从父中继承得到的方法
```

复杂示例

```ts
class Animal {
  name: string
  
  constructor (name: string) {
    this.name = name
  }

  run (distance: number=0) {
    console.log(`${this.name} run ${distance}m`)
  }

}

class Snake extends Animal {
  constructor (name: string) {
    // 调用父类型构造方法
    super(name)
  }

  // 重写父类型的方法
  run (distance: number=5) {
    console.log('sliding...')
    super.run(distance)
  }
}

class Horse extends Animal {
  constructor (name: string) {
    // 调用父类型构造方法
    super(name)
  }

  // 重写父类型的方法
  run (distance: number=50) {
    console.log('dashing...')
    // 调用父类型的一般方法
    super.run(distance)
  }

  xxx () {
    console.log('xxx()')
  }
}

const snake = new Snake('sn')
snake.run()

const horse = new Horse('ho')
horse.run()

// 父类型引用指向子类型的实例 ==> 多态
const tom: Animal = new Horse('ho22')
tom.run()

/* 如果子类型没有扩展的方法, 可以让子类型引用指向父类型的实例 */
const tom3: Snake = new Animal('tom3')
tom3.run()
/* 如果子类型有扩展的方法, 不能让子类型引用指向父类型的实例 */
// const tom2: Horse = new Animal('tom2')
// tom2.run()
```

###### ⑶ 修饰符

> 描述类中的成员(属性,构造函数,方法)的可访问性

- 公共，私有与受保护的修饰符

  ```ts
  /* 
  访问修饰符: 用来描述类内部的属性/方法的可访问性
    public: 默认值, 公开的外部也可以访问
    private: 只能类内部可以访问
    protected: 类内部和子类可以访问
  */
  
  class Animal {
    public name: string
  
    public constructor (name: string) {
      this.name = name
    }
  
    public run (distance: number=0) {
      console.log(`${this.name} run ${distance}m`)
    }
  }
  
  class Person extends Animal {
    private age: number = 18
    protected sex: string = '男'
  
    run (distance: number=5) {
      console.log('Person jumping...')
      super.run(distance)
    }
  }
  
  class Student extends Person {
    run (distance: number=6) {
      console.log('Student jumping...')
  
      console.log(this.sex) // 子类能看到父类中受保护的成员
      // console.log(this.age) //  子类看不到父类中私有的成员
  
      super.run(distance)
    }
  }
  
  console.log(new Person('abc').name) // 公开的可见
  // console.log(new Person('abc').sex) // 受保护的不可见
  // console.log(new Person('abc').age) //  私有的不可见
  ```

- `readonly` 修饰符

  > 你可以使用 `readonly` 关键字将属性设置为只读的。 只读属性必须在声明时或构造函数里被初始化

  ```ts
  class Person {
    // 声明只读  
    readonly name: string = 'abc'  // 初始值
    // 构造函数  
    constructor(name: string) {  
      this.name = name
    }
  }
  
  let john = new Person('John')
  // john.name = 'peter' // error
  ```

  - 参数属性

    > 参数属性可以方便地让我们在一个地方定义并初始化一个成员

    ```ts
    class Person2 {
      constructor(readonly name: string) {
      }
    }
    
    const p = new Person2('jack')
    console.log(p.name)
    ```

###### ⑷ 存取器

> 通过 `getters/setters` 来截取对对象成员的访问。 它能帮助你有效的控制对对象成员的访问。

```ts
class Person {
  firstName: string = 'A'
  lastName: string = 'B'
  // getter  
  get fullName () {
    return this.firstName + '-' + this.lastName
  }
  // setter  
  set fullName (value) {
    const names = value.split('-')
    this.firstName = names[0]
    this.lastName = names[1]
  }
}

const p = new Person()
console.log(p.fullName)

p.firstName = 'C'
p.lastName =  'D'
console.log(p.fullName)

p.fullName = 'E-F'
console.log(p.firstName, p.lastName)
```

###### ⑸ 静态属性

> 到目前为止，我们只讨论了类的实例成员，那些仅当类被实例化的时候才会被初始化的属性。 我们也可以创建类的静态成员，这些属性存在于类本身上面而不是类的实例上

```ts
/* 
静态属性, 是类对象的属性
非静态属性, 是类的实例对象的属性
*/

class Person {
  name1: string = 'A'
  static name2: string = 'B'
}

console.log(Person.name2)
console.log(new Person().name1)
```

###### ⑹ 抽象类

> 抽象类做为其它派生类的基类使用。 它们不能被实例化。不同于接口，抽象类可以包含成员的实现细节。 `abstract`
> 关键字是用于定义抽象类和在抽象类内部定义抽象方法。

```ts
/* 
抽象类
  不能创建实例对象, 只有实现类才能创建实例
  可以包含未实现的抽象方法
*/

abstract class Animal {

  abstract cry ()

  run () {
    console.log('run()')
  }
}

class Dog extends Animal {
  cry () {
    console.log(' Dog cry()')
  }
}

const dog = new Dog()
dog.cry()
dog.run()
```

##### ④ 函数

###### ⑴ 基本示例

```ts
// 命名函数
function add(x, y) {
  return x + y
}

// 匿名函数
let myAdd = function(x, y) { 
  return x + y;
}
```

###### ⑵ 函数类型

- 为函数定义类型

  ```ts
  // 为函数定义类型
  function add(x: number, y: number): number {
    return x + y
  }
  
  let myAdd = function(x: number, y: number): number { 
    return x + y
  }
  ```

- 完整写法

  ```ts
  // 完整写法
  let myAdd2: (x: number, y: number) => number = 
  function(x: number, y: number): number {
    return x + y
  }
  ```

- 可选参数和默认参数

  ```ts
  // 可选参数和默认参数
  function buildName(firstName: string='A', lastName?: string): string {
    if (lastName) {
      return firstName + '-' + lastName
    } else {
      return firstName
    }
  }
  
  console.log(buildName('C', 'D'))
  console.log(buildName('C'))
  console.log(buildName())
  ```

- 剩余参数

  ```ts
  // 剩余参数
  function info(x: string, ...args: string[]) {
    console.log(x, args)
  }
  info('abc', 'c', 'b', 'a')
  ```

- 函数重载

  > 函数重载: 函数名相同, 而形参不同的多个函数

  ```ts
  /* 
  函数重载: 函数名相同, 而形参不同的多个函数
  需求: 我们有一个add函数，它可以接收2个string类型的参数进行拼接，也可以接收2个number类型的参数进行相加 
  */
  
  // 重载函数声明
  function add (x: string, y: string): string
  function add (x: number, y: number): number
  
  // 定义函数实现
  function add(x: string | number, y: string | number): string | number {
    // 在实现上我们要注意严格判断两个参数的类型是否相等，而不能简单的写一个 x + y
    if (typeof x === 'string' && typeof y === 'string') {
      return x + y
    } else if (typeof x === 'number' && typeof y === 'number') {
      return x + y
    }
  }
  
  console.log(add(1, 2))
  console.log(add('a', 'b'))
  // console.log(add(1, 'a')) // error
  ```

##### ⑤ 泛型

> 指在定义函数、接口或类的时候，不预先指定具体的类型，而在使用的时候再指定具体类型的一种特性。

###### ⑴ 引入

```ts
function getArray(value: any, count: number): any[] {
    const arr: any[] = []
    for (let index = 0; index < count; index++) {
        arr.push(value)
    }
    return arr
}

const arr1 = getArray(11, 3)
const arr2 = getArray('aa', 3)
console.log(arr1[0].toFixed(), arr2[0].split(''))
```

###### ⑵ 函数泛型

```ts
function getArray2<T>(value: T, count: number) {
    const arr: Array<T> = []
    for (let index = 0; index < count; index++) {
        arr.push(value)
    }
    return arr
}

const arr3 = getArray2<number>(11, 3)
console.log(arr3[0].toFixed())
// console.log(arr3[0].split('')) // error
const arr4 = getArray2<string>('aa', 3)
console.log(arr4[0].split(''))
// console.log(arr4[0].toFixed()) // error
```

###### ⑶ 多泛型参数的函数

> 一个函数可以定义多个泛型参数

```ts
function swap <K, V> (a: K, b: V): [K, V] {
  return [a, b]
}
const result = swap<string, number>('abc', 123)
console.log(result[0].length, result[1].toFixed())
```

###### ⑷ 泛型接口

```ts
interface IbaseCRUD <T> {
  data: T[]
  add: (t: T) => void
  getById: (id: number) => T
}

class User {
  id?: number; //id主键自增
  name: string; //姓名
  age: number; //年龄

  constructor (name, age) {
    this.name = name
    this.age = age
  }
}

class UserCRUD implements IbaseCRUD <User> {
  data: User[] = []
  
  add(user: User): void {
    user = {...user, id: Date.now()}
    this.data.push(user)
    console.log('保存user', user.id)
  }

  getById(id: number): User {
    return this.data.find(item => item.id===id)
  }
}


const userCRUD = new UserCRUD()
userCRUD.add(new User('tom', 12))
userCRUD.add(new User('tom2', 13))
console.log(userCRUD.data)
```

###### ⑸ 泛型类

> 在定义类时, 为类中的属性或方法定义泛型类型 在创建类的实例时, 再指定特定的泛型类型

```ts
class GenericNumber<T> {
  zeroValue: T
  add: (x: T, y: T) => T
}

let myGenericNumber = new GenericNumber<number>()
myGenericNumber.zeroValue = 0
myGenericNumber.add = function(x, y) {
  return x + y 
}

let myGenericString = new GenericNumber<string>()
myGenericString.zeroValue = 'abc'
myGenericString.add = function(x, y) { 
  return x + y
}

console.log(myGenericString.add(myGenericString.zeroValue, 'test'))
console.log(myGenericNumber.add(myGenericNumber.zeroValue, 12))
```

###### ⑹ 泛型约束

> 如果我们直接对一个泛型参数取 `length` 属性, 会报错, 因为这个泛型根本就不知道它有这个属性

```ts
// 没有泛型约束
function fn <T>(x: T): void {
  // console.log(x.length)  // error
}
```

> 定义一个接口进行约束

```ts
interface Lengthwise {
  length: number;
}

// 指定泛型约束
function fn2 <T extends Lengthwise>(x: T): void {
  console.log(x.length)
}
```

##### ⑥ 其它

###### ⑴ 声明文件

###### ⑵ 内置对象

```ts
/**
 *	Boolean
 *	Number
 *	String
 *	Date
 *	RegExp
 *	Error
 */	

/* 1. ECMAScript 的内置对象 */
let b: Boolean = new Boolean(1)
let n: Number = new Number(true)
let s: String = new String('abc')
let d: Date = new Date()
let r: RegExp = /^1/
let e: Error = new Error('error message')
b = true
// let bb: boolean = new Boolean(2)  // error

/**
 * 	Window
 *	Document
 *	HTMLElement
 *	DocumentFragment
 *	Event
 *	NodeList
 */
const div: HTMLElement = document.getElementById('test')
const divs: NodeList = document.querySelectorAll('div')
document.addEventListener('click', (event: MouseEvent) => {
  console.dir(event.target)
})
const fragment: DocumentFragment = document.createDocumentFragment()
```
