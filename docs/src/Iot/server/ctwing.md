---
title: Ctwing 电信iot平台
index: 4
---

   ![](https://secure2.wostatic.cn/static/87ipgB13vTQcbTJoov8jMW/4c64ca92-549f-49d7-a6a7-36bb98fcfda6.jpg?auth_key=1691052584-gPcBeooHyeGvNSrVnWnZaN-0-ca6ca6efafff4b0c15a952b38308898d)

## 北向接收程序开发(Java)

### 1.硬件连接平台

milsight 



### 2.HTTP订阅

1.  添加完设备以后，设备管理查看 应用订阅 

![](https://secure2.wostatic.cn/static/2o3zS3qMjGtMspAnG83y2G/image.png?auth_key=1691052584-kBDvxrED36SQVNhy1VVBAn-0-a8ae9f0295ec55a1d16f3ea23351ac68)



1. 添加订阅方URL 注：需要线上环境 ，

![](https://secure2.wostatic.cn/static/th3qBQsKDtrcvwUz2SBHp4/image.png?auth_key=1691052584-7C2BZGjAazV9pT2kiC2N3M-0-c40a82d0287a190dc7e08968006f1036)



NodeJs 后台订阅代码 ： 



### 3.MQTT订阅（付费）

MQ 服务则为收费项目 

1.添加 Topic 购买完该 服务之后 下面则会出现购买中填写的 Tipoc 主题

![](https://secure2.wostatic.cn/static/geEnRGUoSL1RC61Xaq8Mng/image.png?auth_key=1691052584-6ZXkZyVFEuoiM6da4xCuyd-0-e00ceaceb77bcb6a26212bf0b80f6ce1)

1. 添加路由 

![](https://secure2.wostatic.cn/static/6V5dduF99CEiJs751yHm3i/image.png?auth_key=1691052584-uctd1iiQ5KdkgDEXL3subu-0-e76ea8f368ea1c2b36360792b6317971)



2.1选择路由配置

![](https://secure2.wostatic.cn/static/gpRWwpeSnqqXpd3GkeTz2b/image.png?auth_key=1691052584-tiN2xYkjhsUaVmGc58NJJw-0-a8cc9dc2975d6dfa457c3f931ca8426a)

2.2 添加数据源 选择绑定的产品设别

![](https://secure2.wostatic.cn/static/7wvE2kmgBjRqBUiZP76GSv/image.png?auth_key=1691052584-fPp6QVZqNK7JThiVtxKGtg-0-f20576faaae5f7ae9e8572eeb069b2ca)

2.3 添加目的地配置

![](https://secure2.wostatic.cn/static/u64sXxoZSYkDwnMchMeZK8/image.png?auth_key=1691052584-42nNL49YnWYWhnVZaeZxh8-0-15484af4836f6ce63a8f065f5abfcc5a)

配置完毕 开发北向应用进行接收



