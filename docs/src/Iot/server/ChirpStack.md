---
title: ChirpStack
index: 5
---

## ChirpStack 简介

ChirpStack 是一款开源的 LoRaWAN 网络服务器，可用于建立私人或公共 LoRaWAN 网。ChirpStack 为网关、设备和租户的管理提供了一个 web 界面，并与主要的云提供商、数据库和通常用于处理设备数据的服务进行了数据集成，主要的集成服务包括: MQTT 前向转发代理、HTTP 转发代理、influxDB入库服务等。ChirpStack 还提供了基于 gRPC 的 API，可用于集成或扩展 ChirpStacck 的各种功能。

### ChirpStack架构

ChirpStack 项目的架构图如下所示:

![](https://pic2.zhimg.com/80/v2-eb03439f9699665275a59c194aef6cbd_720w.webp)

在架构图中，可以看到 LoRa Gateway 与服务端连接的方式有 2 种。一种是由 Packet Forwarder 直接与 ChirpStack Gateway Bridge 连接，另一种在 LoRa 网关内直接集成了 Packet Forwarder 和 ChirpStack Gateway Bridge 这两个组件，然后与 MQTT Broker 进行连接。

## Chirpstack 部署

### Chirpstack 部署方式

Chirpstack 可以以 Docker 方式部署，同时，也可以在 Debian/Ubuntu 服务器上单独部署。在 Debian/Ubuntu 服务器上单独部署，需要先安装依赖包，再进行 Chirpstack 的部署，实际操作中，我觉得比较麻烦，下面就一在 Docker 运行的方式进行部署说明。

运行环境

### 克隆 Github 仓库

Chirpstack 官方提供了一个快速部署 Chirpstack 的 Github 仓库，利用 dockper compose 工具，对 Chirpstack 服务进行快速部署。

  git clone [https://github.com/chirpstack/chirpstack-docker.git](https://github.com/chirpstack/chirpstack-docker.git)

### 启动服务

  cd chirpstack-docker

  docker-compose up -d

### 查看 docker 运行状态

等待 docker 启动完成后，不改变系统路径，直接在命令行输入：

  docker compose ls

命令行返回 compose 启动的情况：

  NAME                STATUS              CONFIG FILES

  chirpstack-docker   running(6)          /home/sage/chirpstack-docker/docker-compose.yml

然后，再查看 docker compose 启动的容器运行情况：

  docker compose ps

命令行返回 compose 启动的情况：

  NAME                STATUS              CONFIG FILES

  chirpstack-docker   running(6)          /home/sage/chirpstack-docker/docker-compose.yml

然后，再查看 docker compose 启动的容器运行情况：

  docker compose ps



