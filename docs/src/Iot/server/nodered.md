---
title: nodered
index: 3
---

             Node-RED 是构建物联网(IOT， Internet of Things)应用程序的一个强大工具，其重点是简化代码块的“连接”以执行任务。它使用可视化编程方法，允许开发人员将预定义的代码块（称为“节点”，Node）连接起来执行任务。连接的节点，通常是输入节点、处理节点和输出节点的组合，当它们连接在一起时，构成一个“流”(Flows)。

              Node-RED最初是IBM在2013年末开发的一个开源项目，以满足他们快速连接硬件和设备到Web服务和其他软件的需求——作为物联网的一种粘合剂，它很快发展成为一种通用的物联网编程工具。重要的是，Node-RED已经迅速形成一个重要的、不断增长的用户基础和一个活跃的开发人员社区，他们正在开发新的节点，同时允许程序员复用Node-RED代码来完成各种各样的任务。

- Node-Red是一个开源的可视化编程工具
- Node-RED由IBM开发，主要用于连接连接计算机、传感器和在线服务等协议或组件，以简化它们之间的布线工作
- Node-RED允许通过组合各部件来编写应用程序。这些部件也可以是硬件设备、Web API 、在线服务


#### npm本地安装（window）

```JavaScript
#全局安装node-red
npm install -g node-red

#安装完成后，启动node-red
node-red

```

启动后在浏览器输入 [http://127.0.0.1:1880](http://127.0.0.1:1880) 就能进入node-red界面了。

#### docker安装node-red（linux）

拉取node-red镜像

```JavaScript
#先下载node-red的docker镜像
docker pull nodered/node-red
```

运行镜像

```JavaScript
#运行镜像
docker run -d --restart always -e TZ="Asia/Shanghai" -p 0.0.0.0:1880:1880 -v /opt/node_red_data:/data --name mynodered nodered/node-red

# --restart always  自动启动
# -e TZ="Asia/Shanghai"  设置时区
# -p 1880:1880  设置node-red端口，:左侧端口号可以改，改完之后，访问地址的端口号也会相应改变
# -v /opt/node_red_data:/data  挂载宿主机路径
# --name mynodered  容器名称
# nodered/node-red  镜像名称，可以指定版本，一般是默认安装最新版本

```

查看nodered容器是否安装成功

```JavaScript
docker ps | grep nodered
```

#### 启用身份验证

现在已经可以正常进入node-red界面，但是目前是没有开启身份验证的，就是说谁都可以访问，接下来需要启用身份验证：

```JavaScript
# 首先要保证容器正在运行，然后要为使用的密码生成哈希值
docker exec -it mynodered npx node-red admin hash-pw

```

         运行这个命令后，会出现提示Password：，在这里输入你要设定的密码，密码只需要输入一次，没有二次确认，也不会显示出来。如果忘记了密码，可以重新运行这个命令，进行密码重置。

输入完密码后，会出现一行哈希码，需要把这个哈希码记住，然后粘贴到setting.js文件中。

setting.js文件的位置在配置的挂载宿主机路径下的_data文件夹下，挂载宿主机路径在运行镜像的时候设置过，就比如我设置的路径是/opt/node_red_data:/data ，所以setting.js文件的存放路径是/opt/node_red_data/setting.js。

找到setting.js文件后，使用编辑软件打开文件，然后找到被注释的以下代码：

```JavaScript
/** To password protect the Node-RED editor and admin API, the following
 * property can be used. See http://nodered.org/docs/security.html for details.
 */
// adminAuth: {
//    type: "credentials",
//    users: [{
//        username: "admin",
//        password: "$2b$08$A5iFyOc477qQUy5pbNmx2ucFiULfX52JrE3VPhSDUNMisJyy9Sdim",
//        permissions: "*"
//    }]
// },

```

  把adminAuth的注释取消，然后将刚才生成的哈希码复制到password里面，username改为你设置的用户名就可以了，当然也可以在users这个数字里面新增一个用户，密码的设置方式同上。以下为示例：

```JavaScript
/** To password protect the Node-RED editor and admin API, the following
 * property can be used. See http://nodered.org/docs/security.html for details.
 */
adminAuth: {
   type: "credentials",
   users: [{
       username: "admin",//账户名
       password: "$2b$08$A5iFyOc477qQUy5pbNmx2ucFiULfX52JrE3VPhSDUNMisJyy9Sdim", // 设置密码生成的哈希值
       permissions: "*" // *-完全访问   read-只读访问
   },{
       username: "test",//账户名
       password: "$2b$08$WMUbJUVLwEQuZRwDgSi4d.0/sa5FS8LytojO6gG53vxDncgo8SC9C", // 设置密码生成的哈希值
       permissions: "read" // *-完全访问   read-只读访问
   },]
}


```

设置完之后，重启一下node-red

```JavaScript
#获取容器ID
docker ps | grep nodered

#重启容器
docker restart 容器ID

```

现在，访问 服务器地址:配置的端口 就可以看到部署好的node-red界面，输入配置好的账号和密码，就可以正常使用了。