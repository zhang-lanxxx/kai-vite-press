---
title: EMQX
index: 1
---

# EMQX 高可用的分布式 MQTT 消息服务器

![](image/image_FSLUEqWfWX.png)

&#x20;     EMQ X （简称 EMQ）， 是一款完全[开源](https://baike.baidu.com/item/开源/20720669?fromModule=lemma_inlink "开源")，高度可伸缩，高可用的分布式 [MQTT](https://baike.baidu.com/item/MQTT/3618851?fromModule=lemma_inlink "MQTT") [消息服务器](https://baike.baidu.com/item/消息服务器/568190?fromModule=lemma_inlink "消息服务器")，同时也支持 [CoAP](https://baike.baidu.com/item/CoAP/6469713?fromModule=lemma_inlink "CoAP")/LwM2M 一站式 IoT 协议接入。EMQ 是 [5G](https://baike.baidu.com/item/5G/29780?fromModule=lemma_inlink "5G") 时代万物互联的消息引擎，适用于 [IoT](https://baike.baidu.com/item/IoT/552548?fromModule=lemma_inlink "IoT")、M2M 和移动应用程序，可处理千万级别的并发客户端。 \[1]

Docker 安装EMQX

1.  下载镜像

```bash
docker pull emqx/emqx
```

&#x20;2\. 后台运行镜像&#x20;

```bash
docker run -dit --name emqx -p 18083:18083 -p 1883:1883 -p 8083:8083 -p 8084:8084 --restart=always emqx/emqx:latest
```

1.  访问emqt的web管理页面

```bash
http://ip:18083
#账号： admin
#密码: public
```

1.  端口

```bash
1883：MQTT 协议端口
8883：MQTT/SSL 端口
8083：MQTT/WebSocket 端口
8080：HTTP API 端口
18083：Dashboard 管理控制台端口
```

PS: 注意开发 端口 以及端口号冲突&#x20;

1.  来到首页选择

![](image/image_kDwnXI8-lA.png)

2.添加主题

![](image/image_oDtj-GfB4p.png)

3.连接管理查看连接状态

![](image/image_kv3c8ElR7M.png)

### 后台订阅代码&#x20;

[<https://gitee.com/zhang-lanxxx/smart> *-mqtt*-api](https://gitee.com/zhang-lanxxx/smart_-mqtt_-api)
