---
title: Vue 在控制 容器滚动盒子以及鼠标移入移出控制
index: 2
---

```js
    scrollToTopSmooth(scrollDom, speed) {
      const that = this;
      let { scrollTop } = scrollDom;
      function move() {
        scrollTop -= speed;
        // eslint-disable-next-line no-param-reassign
        scrollDom.scrollTop = scrollTop;
        if (scrollTop > 0) {
          that.timer = window.requestAnimationFrame(move);
        } else {
          // eslint-disable-next-line no-param-reassign
          scrollDom.scrollTop = 0;
        }
      }
      move();
    },
```

```js
    scrollToBottomSmooth(scrollDom, speed) {
      const that = this;
      let { scrollTop } = scrollDom;
      function move() {
        scrollTop += speed;
        // eslint-disable-next-line no-param-reassign
        scrollDom.scrollTop = scrollTop;
        if (scrollTop > 0) {
          that.timer = window.requestAnimationFrame(move);
        } else {
          // eslint-disable-next-line no-param-reassign
          scrollDom.scrollTop = 0;
        }
      }
      move();
    },
```

**使用**

```js
      const myDiv = document.getElementById("myDiv");
      this.scrollToBottomSmooth(myDiv, 5);
```

**鼠标移出停止清楚定时器**

 ```js
     handleLeave() {
       if (this.timer) {
         window.cancelAnimationFrame(this.timer);
       }
     },
 ```