---
title: ResizeObserver & v-if
index: 2
---

#### 1. Vue 在使用v-if的前提下，使用ResizeObserver配合ref获取元素块的高度



遇到的问题

#### 1. 在computed中没法操作dom元素，比如

```JS
computed: {
	w() {
		return  this.$refs.box.offsetWidth
	}
}
```

运行中报undefined错误，后来改成在mounted中获取this.$refs.box.offsetWidth,后来发现，在computed中，DOM元素未加载下来，保险起见，还是在mounted操作DOM

### 最终方案

在watch里面监听容器显示变化，在v-if 的值为 true的时候，调用监听方法

```js
  watch: {
    // Init 控制容器的显示与隐藏
    Init(val) {
      if (val) {
        this.$nextTick(() => {
          this.handleWatchElement();
        });
      }
    },
  },
```

关键监听   `ResizeObserver()`

```js
    handleWatchElement() {
      const divElem = this.$refs.myDiv; //  监听元素
      if (divElem) {
        const resizeObserver = new ResizeObserver(() => {
          if (divElem.scrollHeight > divElem.clientHeight && !this.IsScroll) {
            this.IsScroll = true; // 该元素有滚动条
          } else if (divElem.scrollHeight <= divElem.clientHeight) {
            this.IsScroll = false;
          }
        });
        resizeObserver.observe(divElem);
      }
    },
```