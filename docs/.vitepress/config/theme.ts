/*
 * @description: 
 * @Date: 2023-07-24 14:23:10
 * @example: 
 * @params: 
 */
import type {DefaultTheme} from 'vitepress'
import {local} from './search'
import buildBar from "../utils/convert";

/**
 * 主题配置
 */
export const themeConfig: DefaultTheme.Config = {
  // 导航栏
  nav: buildBar(),
  // 侧边栏
  sidebar: buildBar(true),
  // sidebar,
  // 左上角logo
  logo: '/logo.svg',
  // 国际化跳转
  i18nRouting: false,
  // 大纲显示
  outline: {
    level: 'deep', // 大纲标题层级
    label: '快速访问' //大纲标题显示
  },
  // 主题切换文案
  darkModeSwitchLabel: '切换主题',
  // 侧边栏展开按钮显示文案
  sidebarMenuLabel: '文章',
  // 返回顶部展示文案
  returnToTopLabel: '返回顶部',
  // 最后更新时间文本显示 , 需设置 lastUpdate:true
  lastUpdatedText: '最后更新',
  // 页脚向上向下翻页展示
  docFooter: {
    // 跳转上个页面显示文案
    prev: '上一篇',
    // 跳转下个页面显示文案
    next: '下一篇'
  },
  // 搜索
  search: {
    // 本地搜索
    provider: 'local',
    options: local,
    // provider: 'algolia',
    // options: algolia
  },
  // 编辑页面配置
  // editLink: {pattern: '', text: ''},
  // 社交链接配置
  socialLinks: [
    {
      icon: {
        svg: '<svg role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>码云</title><path d="M11.984 0A12 12 0 0 0 0 12a12 12 0 0 0 12 12 12 12 0 0 0 12-12A12 12 0 0 0 12 0a12 12 0 0 0-.016 0zm6.09 5.333c.328 0 .593.266.592.593v1.482a.594.594 0 0 1-.593.592H9.777c-.982 0-1.778.796-1.778 1.778v5.63c0 .327.266.592.593.592h5.63c.982 0 1.778-.796 1.778-1.778v-.296a.593.593 0 0 0-.592-.593h-4.15a.592.592 0 0 1-.592-.592v-1.482a.593.593 0 0 1 .593-.592h6.815c.327 0 .593.265.593.592v3.408a4 4 0 0 1-4 4H5.926a.593.593 0 0 1-.593-.593V9.778a4.444 4.444 0 0 1 4.445-4.444h8.296Z"/></svg>'
      },
      link: 'https://gitee.com/zhang-lanxxx'
    },
  ],
  // 页脚配置
  footer: {
    message: '记录当下知识',
    // 版权
    copyright: 'Copyright © 2023-present x0e'
  },

}


