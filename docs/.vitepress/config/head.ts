import type {HeadConfig} from "vitepress";
import {metaData} from "./consts/meta";

/**
 * head 配置
 */
export const head: HeadConfig[] = [
  ['link', {rel: 'icon', href: '/icon.png'}],
  ['meta', {name: 'author', content: 'x0e'}],
  ['meta', {name: 'keywords', content: '"个人博客","blog", "x0e", "技术分享","nil","渣码"'}],
  ['meta', {name: 'HandheldFriendly', content: 'True'}],
  ['meta', {name: 'MobileOptimized', content: '320'}],
  ['meta', {name: 'theme-color', content: '#3c8772'}],
  ['meta', {property: 'og:type', content: 'website'}],
  ['meta', {property: 'og:type', content: 'website'}],
  ['meta', {property: 'og:locale', content: metaData.locale}],
  ['meta', {property: 'og:title', content: metaData.title}],
  ['meta', {property: 'og:description', content: metaData.description}],
  ['meta', {property: 'og:site', content: metaData.site}],
  ['meta', {property: 'og:site_name', content: metaData.title}],
  ['meta', {property: 'og:image', content: metaData.image}],
]
