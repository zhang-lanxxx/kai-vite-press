/*
 * @description: 
 * @Date: 2023-07-24 14:23:10
 * @example: 
 * @params: 
 */
const site: string = "https://dodoo.co"

export const metaData = {
  lang: 'zh-CN',
  locale: 'zh_CN',
  title: "Kai", // 站点标题
  titleTemplate: '记录片段,见证成长! - :title', // 标题模板
  description: "Recording!", // 站点描述
  site,
  image: `${site}/logo.jpg`,
}
