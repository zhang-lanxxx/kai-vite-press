import type {MarkdownOptions} from 'vitepress'
import MarkdownIt from 'markdown-it/lib'

/**
 * markdown 配置项
 */
export const markdown: MarkdownOptions = {
  // markdown 主题配置
  // 参考地址 : https://github.com/shikijs/shiki/blob/main/docs/themes.md
  theme: {
    // 暗黑模式主题
    // light: 'one-dark-pro',
    light: 'one-dark-pro',
    // 明亮模式主题
    dark: 'github-dark'
  },
  // 启用行号
  lineNumbers: false,
  // 重写配置
  config: (md: MarkdownIt): void => {
    // md.use();
  }
}
