// https://vitepress.dev/guide/custom-theme
import DefaultTheme from 'vitepress/theme'
import './style.css'
import {EnhanceAppContext} from 'vitepress';
import CustomLayout from "./CustomLayout.vue";

if (typeof window !== 'undefined') {
  /* 注销 PWA 服务 */
  if (window.navigator && navigator.serviceWorker) {
    navigator.serviceWorker.getRegistrations().then(function (registrations) {
      for (let registration of registrations) {
        registration.unregister().then(r => {
        })
      }
    })
  }

  /* 删除浏览器中的缓存 */
  if ('caches' in window) {
    caches.keys().then(function (keyList) {
      return Promise.all(
        keyList.map(function (key) {
          return caches.delete(key)
        })
      )
    })
  }
}


export default {
  ...DefaultTheme,
  // Layout: () => {
  //     const props: Record<string, any> = {}
  //     const {frontmatter} = useData();
  //     return h(DefaultTheme.Layout, props, {
  //         // https://vitepress.dev/guide/extending-default-theme#layout-slots
  //     })
  // },
  Layout: CustomLayout,
  enhanceApp(ctx: EnhanceAppContext) {
    DefaultTheme.enhanceApp(ctx)
    // ...
  }
}
