import {defineConfig} from 'vitepress'
import {markdown} from './config/markdown'
import {metaData} from './config/consts/meta'
import {head} from './config/head'
import {themeConfig} from "./config/theme"
import Components from "unplugin-vue-components/vite";

/**
 * vitepress 配置
 *
 * 参考文档 : https://vitepress.dev/reference/site-config
 */
export default defineConfig({
  lang: metaData.lang,
  title: metaData.title, // 站点标题
  titleTemplate: metaData.titleTemplate, // 标题模板
  description: metaData.description, // 站点描述
  srcDir: 'src', // 修改源目录
  cleanUrls: true, // 去除 .md .html 后缀
  base:"/kai-vite-press/",
  ignoreDeadLinks: true, // 忽略死链接,防止build失败
  lastUpdated: true, // 显示最近更新时间
  markdown: markdown, // markdown 配置
  head: head, // <head> 标签配置
  themeConfig: themeConfig,// 主题配置
  // vite 配置
  vite: {
    plugins: [
      Components({
        dirs: ['../.vitepress/theme/components'],
        include: [/\.vue$/, /\.vue\?vue/, /\.md$/],
        dts: '../.vitepress/types/components.d.ts'
      }),
    ],
  },
  // 重写路径
  rewrites: {
    // '/article/issue/**/*.md': '/issue/**/index.md.md.md',
    '/article/:page': ':page'
  },
  // html 转换钩子
  transformHtml: (code, id, ctx) => {
    // console.log(code)
    // console.log(id)
    // console.log(ctx)
  },
  // 构建结束钩子
  buildEnd: async (siteConfig) => {
    // console.log(siteConfig)
    // const posts = await createContentLoader('../docs/src/**/*.md', {
    //     includeSrc: true,
    //     render: true,
    //     excerpt: true,
    //     transform(row) {
    //         console.log('----------------'+row)
    //         return row.filter(({url}) => {
    //             return !url.match('about|archives|categories|tags|template')
    //         }).map(({url, frontmatter, excerpt}) => ({
    //             title: frontmatter.title,
    //             url,
    //             excerpt,
    //             index.md.md: frontmatter.index.md.md,
    //             isTop: frontmatter.isTop
    //         })).sort((before, after) => after.index.md.md - before.index.md.md)
    //     }
    // }).load()
    // themeConfig.sidebar
    // themeConfig.nav
  },
  transformPageData: (pageData, ctx) => {
    // console.log(pageData)
    // console.log(ctx)
  }
})
