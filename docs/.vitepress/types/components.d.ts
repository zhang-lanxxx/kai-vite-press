/* eslint-disable */
/* prettier-ignore */
// @ts-nocheck
// Generated by unplugin-vue-components
// Read more: https://github.com/vuejs/core/pull/3399
import '@vue/runtime-core'

export {}

declare module '@vue/runtime-core' {
  export interface GlobalComponents {
    ArticleTagDisplay: typeof import('./../theme/components/ArticleTagDisplay.vue')['default']
    NavLink: typeof import('./../theme/components/NavLink.vue')['default']
  }
}
